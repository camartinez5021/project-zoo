package com.zoo.zoo.domain;


import com.zoo.zoo.domain.enumeration.GenderType;

import javax.persistence.*;

@Entity
public class Animals {

    @Id
    private String code;

    private String name;
    private String race;

    @Enumerated(EnumType.STRING)
    private GenderType genderType;

    @ManyToOne
    private Locations locations;

    public Animals() {
    }

    public Animals(String code,String name, String race, GenderType genderType, Locations locations){
        this.code = code;
        this.name = name;
        this.race = race;
        this.genderType = genderType;
        this.locations = locations;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public GenderType getGenderType() {
        return genderType;
    }

    public void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }

    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }
}
