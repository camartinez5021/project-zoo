package com.zoo.zoo.domain;

import javax.persistence.*;

@Entity
public class Locations {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String description;

    @ManyToOne
    private LocationsType locationsType;

    public Locations(){

    }

    public Locations(String description, LocationsType locationsType) {
        this.description = description;
        this.locationsType = locationsType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocationsType getLocationsType() {
        return locationsType;
    }

    public void setLocationsType(LocationsType locationsType) {
        this.locationsType = locationsType;
    }
}
