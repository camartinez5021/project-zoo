package com.zoo.zoo.domain.enumeration;

public enum GenderType {
    MALE, FEMALE
}
