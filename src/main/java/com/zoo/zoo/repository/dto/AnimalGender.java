package com.zoo.zoo.repository.dto;

public interface AnimalGender {

    public String getName();
}
