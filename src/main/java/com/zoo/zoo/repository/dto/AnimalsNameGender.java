package com.zoo.zoo.repository.dto;

public interface AnimalsNameGender {

    public String getName();
    public String getGenderType();
}
