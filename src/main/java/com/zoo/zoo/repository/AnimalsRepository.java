package com.zoo.zoo.repository;

import com.zoo.zoo.domain.Animals;
import com.zoo.zoo.repository.dto.AnimalGender;
import com.zoo.zoo.repository.dto.AnimalsNameGender;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface AnimalsRepository extends CrudRepository<Animals, String> {

    //Cuenta total de animales
    @Query(value = "select count(animals) from Animals animals")
    Integer quantityAnimals();

    //Lista de animales por nombre
    @Query(value = "select animals.name as name, animals.genderType as genderType from Animals animals")
    Iterable<AnimalsNameGender> findAnimalsNameGender();

    //Lista de los animales por genero MALE
    @Query(value = "select animals.name as name, animals.genderType as genderType from Animals animals where genderType = 'MALE'")
    Iterable<AnimalGender> findAnimalsMale();

    //Lista de los animales por genero FEMALE
    @Query(value = "select animals.name as name, animals.genderType as genderType from Animals animals where genderType = 'FEMALE'")
    Iterable<AnimalGender> findAnimalsFemale();

    //Cuenta de animales machos
    @Query(value = "select count (genderType) from Animals where genderType = 'MALE'")
    Integer quantityAnimalsMale();

    //Cuenta de animales hembras
    @Query(value = "select count (genderType) from Animals where genderType = 'FEMALE'")
    Integer quantityAnimalsFemale();

}
