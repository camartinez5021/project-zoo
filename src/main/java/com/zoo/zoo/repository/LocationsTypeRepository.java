package com.zoo.zoo.repository;

import com.zoo.zoo.domain.LocationsType;
import org.springframework.data.repository.CrudRepository;

public interface LocationsTypeRepository extends CrudRepository<LocationsType, Integer> {
}
