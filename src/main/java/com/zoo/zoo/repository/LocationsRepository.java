package com.zoo.zoo.repository;

import com.zoo.zoo.domain.Locations;
import org.springframework.data.repository.CrudRepository;

public interface LocationsRepository extends CrudRepository<Locations, Integer> {
}
