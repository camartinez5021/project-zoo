package com.zoo.zoo.service;

import com.zoo.zoo.domain.Locations;
import org.springframework.http.ResponseEntity;

public interface ILocationsService {

    public ResponseEntity create(Locations locations);

    public Iterable<Locations> read();
}
