package com.zoo.zoo.service;

import com.zoo.zoo.domain.Locations;
import com.zoo.zoo.repository.LocationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ILocationsServiceImp implements ILocationsService{

    @Autowired
    private LocationsRepository locationsRepository;

    @Override
    public ResponseEntity create(Locations locations) {
        if(locationsRepository.findById(locations.getId()).isPresent()){
            return new ResponseEntity("Id is use", HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity(locationsRepository.save(locations), HttpStatus.OK);
        }
    }

    @Override
    public Iterable<Locations> read() {
        return locationsRepository.findAll();
    }

}
