package com.zoo.zoo.service;

import com.zoo.zoo.domain.Animals;
import com.zoo.zoo.repository.dto.AnimalGender;
import com.zoo.zoo.repository.dto.AnimalsNameGender;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IAnimalsService {

    public ResponseEntity create(Animals animals);

    public Iterable<Animals> read();

    public Animals update(Animals animals);

    public Optional<Animals> getById(String code);

    public Integer quantityAnimals();

    public Iterable<AnimalsNameGender> getAnimalsNameGender();

    public Iterable<AnimalGender> getAnimalsMale();

    public Iterable<AnimalGender> getAnimalsFemale();

    public Integer quantityAnimalsMale();

    public Integer quantityAnimalsFemale();


}
