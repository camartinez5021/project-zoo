package com.zoo.zoo.service;

import com.zoo.zoo.domain.Animals;
import com.zoo.zoo.repository.AnimalsRepository;
import com.zoo.zoo.repository.dto.AnimalGender;
import com.zoo.zoo.repository.dto.AnimalsNameGender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IAnimalsServiceImp implements IAnimalsService{

    @Autowired
    private AnimalsRepository animalsRepository;

    @Override
    public ResponseEntity create(Animals animals) {

        if(animalsRepository.findById(animals.getCode()).isPresent()){
            return new ResponseEntity("This code is used", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(animalsRepository.save(animals), HttpStatus.OK);
        }
    }

    @Override
    public Iterable<Animals> read() {
        return animalsRepository.findAll();
    }

    @Override
    public Animals update(Animals animals) {
        return animalsRepository.save(animals);
    }

    @Override
    public Optional<Animals> getById(String code) {
        return animalsRepository.findById(code);
    }

    @Override
    public Integer quantityAnimals() {
        return animalsRepository.quantityAnimals();
    }

    @Override
    public Iterable<AnimalsNameGender> getAnimalsNameGender() {
        return animalsRepository.findAnimalsNameGender();
    }

    @Override
    public Iterable<AnimalGender> getAnimalsMale() {
        return animalsRepository.findAnimalsMale();
    }

    @Override
    public Iterable<AnimalGender> getAnimalsFemale() {
        return animalsRepository.findAnimalsFemale();
    }

    @Override
    public Integer quantityAnimalsMale() {
        return animalsRepository.quantityAnimalsMale();
    }


    @Override
    public Integer quantityAnimalsFemale() {
        return animalsRepository.quantityAnimalsFemale();
    }


}
