package com.zoo.zoo.service;

import com.zoo.zoo.domain.LocationsType;
import com.zoo.zoo.repository.LocationsTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ILocationsTypeServiceImp implements ILocationsTypeService {

    @Autowired
    private LocationsTypeRepository locationsTypeRepository;

    @Override
    public ResponseEntity create(LocationsType locationsType) {
        if(locationsTypeRepository.findById(locationsType.getId()).isPresent()){
            return new ResponseEntity("Id is use", HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity(locationsTypeRepository.save(locationsType), HttpStatus.OK);
        }
}

    @Override
    public Iterable<LocationsType> read() {
        return locationsTypeRepository.findAll();
    }
}
