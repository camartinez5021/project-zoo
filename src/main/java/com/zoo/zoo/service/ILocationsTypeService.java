package com.zoo.zoo.service;

import com.zoo.zoo.domain.LocationsType;
import org.springframework.http.ResponseEntity;

public interface ILocationsTypeService {

    public ResponseEntity create(LocationsType locationsType);

    public Iterable<LocationsType> read();
}
