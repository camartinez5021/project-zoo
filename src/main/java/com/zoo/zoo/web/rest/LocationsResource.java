package com.zoo.zoo.web.rest;

import com.zoo.zoo.domain.Locations;
import com.zoo.zoo.domain.LocationsType;
import com.zoo.zoo.service.ILocationsService;
import com.zoo.zoo.service.ILocationsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/locations")
//@RequestMapping("api/locations")
public class LocationsResource {

    @Autowired
    ILocationsService locationsService;

    @PostMapping("")
    //@PostMapping("")
    public ResponseEntity create(@RequestBody Locations locations){
        return locationsService.create(locations);
    }

    @GetMapping("")
    public Iterable<Locations> read(){
        return locationsService.read();
    }
}