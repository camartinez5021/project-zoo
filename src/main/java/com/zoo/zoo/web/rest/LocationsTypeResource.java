package com.zoo.zoo.web.rest;

import com.zoo.zoo.domain.LocationsType;
import com.zoo.zoo.service.ILocationsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/locations-type")
public class LocationsTypeResource {

    @Autowired
    ILocationsTypeService locationsTypeService;

    @PostMapping("")
    public ResponseEntity create(@RequestBody LocationsType locationsType){
        return locationsTypeService.create(locationsType);
    }

    @GetMapping("")
    public Iterable<LocationsType> read(){
        return locationsTypeService.read();
    }
}
