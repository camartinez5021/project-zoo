package com.zoo.zoo.web.rest;

import com.zoo.zoo.domain.Animals;
import com.zoo.zoo.repository.dto.AnimalGender;
import com.zoo.zoo.repository.dto.AnimalsNameGender;
import com.zoo.zoo.service.IAnimalsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/animals")
public class AnimalsResource {

    @Autowired
    IAnimalsService animalsService;

    @PostMapping("")
    public ResponseEntity create(@RequestBody Animals animals){
        return animalsService.create(animals);
    }

    @GetMapping("")
    public Iterable<Animals> read(){
        return animalsService.read();
    }

    @PutMapping("")
    public Animals update(@RequestBody Animals animals) {
        return animalsService.update(animals);
    }

    //Get only one
    @GetMapping("/{id}")
    public Optional<Animals> getById(@PathVariable String code) {
        return animalsService.getById(code);
    }

    //Get count all ANimals
    @GetMapping("/count")
    public Integer countAnimals(){
        return animalsService.quantityAnimals();
    }

    @GetMapping("/name-gender")
    public Iterable<AnimalsNameGender> getAnimalsNameGender(){
        return animalsService.getAnimalsNameGender();
    }

    @GetMapping("/animals-male")
    public Iterable<AnimalGender> getAnimalMale(){
        return animalsService.getAnimalsMale();
    }

    @GetMapping("/animals-female")
    public Iterable<AnimalGender> getAnimalFemale(){
        return animalsService.getAnimalsFemale();
    }

    @GetMapping("/count-male")
    public Integer countAnimalMale(){
        return animalsService.quantityAnimalsMale();
    }

    @GetMapping("/count-female")
    public Integer countAnimalFemale(){
        return animalsService.quantityAnimalsFemale();
    }

}
